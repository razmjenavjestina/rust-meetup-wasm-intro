extern crate yew;
extern crate failure;
extern crate serde;
extern crate serde_derive;
extern crate num_format;

use num_format::{Locale, ToFormattedString};
use failure::Error;
use serde_derive::{Deserialize, Serialize};
use yew::format::{Json, MsgPack, Nothing, Toml, Yaml};
use yew::services::fetch::{FetchService, FetchTask, Request, Response};
use yew::{html, Component, ComponentLink, Html, Renderable, ShouldRender};

pub enum Format {
    Json,
    Toml,
    Yaml,
    MsgPack,
}

pub struct Model {
    fetch_service: FetchService,
    link: ComponentLink<Model>,
    fetching: bool,
    data: Option<DataFromFile>,
    ft: Option<FetchTask>,
}

pub enum Msg {
    FetchData(Format),
    FetchReady(Result<DataFromFile, Error>),
    Ignore,
}

#[derive(Serialize, Deserialize)]
pub struct DataFromFile {
    name: String,
    population: u32,
    country: String,
}

impl Component for Model {
    type Message = Msg;
    type Properties = ();

    fn create(_: Self::Properties, link: ComponentLink<Self>) -> Self {
        Model {
            fetch_service: FetchService::new(),
            link,
            fetching: false,
            data: None,
            ft: None,
        }
    }

    fn update(&mut self, msg: Self::Message) -> ShouldRender {
        match msg {
            Msg::FetchData(format) => {
                self.fetching = true;
                let task = match format {
                    Format::Json => {
                        let callback = self.link.send_back(
                            move |response: Response<Json<Result<DataFromFile, Error>>>| {
                                let (meta, Json(data)) = response.into_parts();
                                if meta.status.is_success() {
                                    Msg::FetchReady(data)
                                } else {
                                    Msg::Ignore
                                }
                            },
                        );
                        let request = Request::get("/zagreb.json").body(Nothing).unwrap();
                        self.fetch_service.fetch_binary(request, callback)
                    }
                    Format::Yaml => {
                        let callback = self.link.send_back(
                            move |response: Response<Yaml<Result<DataFromFile, Error>>>| {
                                let (meta, Yaml(data)) = response.into_parts();
                                if meta.status.is_success() {
                                    Msg::FetchReady(data)
                                } else {
                                    Msg::Ignore
                                }
                            },
                        );
                        let request = Request::get("/tokyo.yml").body(Nothing).unwrap();
                        self.fetch_service.fetch_binary(request, callback)
                    }
                    Format::Toml => {
                        let callback = self.link.send_back(
                            move |response: Response<Toml<Result<DataFromFile, Error>>>| {
                                let (meta, Toml(data)) = response.into_parts();
                                if meta.status.is_success() {
                                    Msg::FetchReady(data)
                                } else {
                                    Msg::Ignore
                                }
                            },
                        );
                        let request = Request::get("/berlin.toml").body(Nothing).unwrap();
                        self.fetch_service.fetch_binary(request, callback)
                    }
                    Format::MsgPack => {
                        let callback = self.link.send_back(
                            move |response: Response<MsgPack<Result<DataFromFile, Error>>>| {
                                let (meta, MsgPack(data)) = response.into_parts();
                                if meta.status.is_success() {
                                    Msg::FetchReady(data)
                                } else {
                                    Msg::Ignore
                                }
                            },
                        );
                        let request = Request::get("/seoul.msgpack").body(Nothing).unwrap();
                        self.fetch_service.fetch_binary(request, callback)
                    }
                };
                self.ft = Some(task);
            }
            Msg::FetchReady(response) => {
                self.fetching = false;
                self.data = response.ok();
            }
            Msg::Ignore => {
                return false;
            }
        }
        true
    }
}

impl Renderable<Model> for Model {
    fn view(&self) -> Html<Self> {
        html! {
            <div>
                <nav class="menu",>
                    <button onclick=|_| Msg::FetchData(Format::Json),>{ "Zagreb [json]" }</button>
                    <button onclick=|_| Msg::FetchData(Format::Toml),>{ "Berlin [toml]" }</button>
                    <button onclick=|_| Msg::FetchData(Format::Yaml),>{ "Tokyo [yaml]" }</button>
                    <button onclick=|_| Msg::FetchData(Format::MsgPack),>{ "Seoul [msgpack]" }</button>
                    { self.view_data() }
                </nav>
            </div>
        }
    }
}

impl Model {
    fn view_data(&self) -> Html<Model> {
        if let Some(data) = &self.data {
            html! {
                <table>
                    <tr>
                    <td>{ "City name" }</td>
                    <td>{ format!("{}", data.name) }</td>
                    </tr>
                    <tr>
                    <td>{ "Population" }</td>
                    <td>{ format!("{}", data.population.to_formatted_string(&Locale::en)) }</td>
                    </tr>
                    <tr>
                    <td>{ "Country" }</td>
                    <td>{ format!("{}", data.country) }</td>
                    </tr>
                 </table>
            }
        } else {
            html! {
                <p>{ "Data hasn't fetched yet." }</p>
            }
        }
    }
}
