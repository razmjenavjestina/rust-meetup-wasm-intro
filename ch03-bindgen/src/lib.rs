extern crate futures;
extern crate js_sys;
extern crate wasm_bindgen;
extern crate wasm_bindgen_futures;

use futures::{future, Future};
use js_sys::Promise;
use serde::{Deserialize, Serialize};
use wasm_bindgen::prelude::*;
use wasm_bindgen::JsCast;
use wasm_bindgen_futures::{future_to_promise, JsFuture};
use web_sys::{Request, RequestInit, RequestMode, Response};

const API_KEY: &str = "xxxxxxxxxxxxxxxxxxxxxxxx";

#[derive(Debug, Serialize, Deserialize)]
pub struct Data {
    id: i32,
    coord: Option<Coord>,
    name: String,
    base: Option<String>,
    sys: Option<Sys>,
    main: Option<Info>,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct Coord {
    lon: Option<f32>,
    lat: Option<f32>,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct Info {
    temp: Option<f32>,
    pressure: Option<i32>,
    humidity: Option<i32>,
    temp_min: Option<f32>,
    temp_max: Option<f32>,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct Sys {
    id: Option<i32>,
    country: Option<String>,
}

#[wasm_bindgen]
pub fn req(city: &str) -> Promise {
    let mut opts = RequestInit::new();
    opts.method("GET");
    opts.mode(RequestMode::Cors);

    let r = &format!(
        "http://api.openweathermap.org/data/2.5/weather?q={}&appid={}",
        city, API_KEY,
    );
    log(r);
    let request = Request::new_with_str_and_init(r, &opts).unwrap();
    request
        .headers()
        .set("Accept", "application/json; charset=utf-8")
        .unwrap();
    let window = web_sys::window().unwrap();
    let request_promise = window.fetch_with_request(&request);
    let future = JsFuture::from(request_promise)
        .and_then(|resp_value| {
            assert!(resp_value.is_instance_of::<Response>());
            let resp: Response = resp_value.dyn_into().unwrap();
            resp.json()
        })
        .and_then(|json_value: Promise| JsFuture::from(json_value))
        .and_then(|json| {
            log(&format!("raw = {:?}", json));
            let data: Result<Data, _> = json.into_serde();
            match data {
                Ok(data) => {
                    log(&format!("response = {:?}", data));
                    future::ok(JsValue::from_serde(&data).unwrap())
                }
                Err(_) => {
                    log("no city");
                    future::err(JsValue::from_str("city not found"))
                }
            }
        });

    future_to_promise(future)
}

#[wasm_bindgen]
extern "C" {
    #[wasm_bindgen(js_namespace = console)]
    fn log(s: &str);
}
