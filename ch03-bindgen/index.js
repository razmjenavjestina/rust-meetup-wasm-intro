const wasm = import('./pkg/testing');

wasm
    .then(h => {
        const searchBtn = document.getElementById('search');
        const resultDiv = document.getElementById('result');
        const cityInput = document.getElementById('city');

        searchBtn.addEventListener('click', () => {
            const city = cityInput.value;
            console.log("Search city", city);
            h.req(city).then((data) => {
                resultDiv.innerHTML = JSON.stringify(data);
            }).catch(function(err) {
                resultDiv.innerHTML = err;
                console.error("catch", err);
            });
        });
    })
    .catch(console.error);
