#[no_mangle]
pub extern "C" fn pow_plus_ten(n: i32) -> i32 {
   pow_two(n) + 10
}

fn pow_two(n: i32) -> i32 {
    n * n
}
