(module
  (func $pow_two (param $n i32) (result i32)
    (get_local $n)
    (get_local $n)
    (i32.mul)
  )
  ;; (n * n) + 10
  (func $pow_plus_ten (param $n i32) (result i32)
    (call $pow_two (get_local $n))
    (i32.const 10)
    (i32.add)
  )
  (export "pow_plus_ten" (func $pow_plus_ten))
)
