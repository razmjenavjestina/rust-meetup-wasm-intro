* [naslov] WebAssembly <3 Rust
* Sto je [[https://webassembly.org/][WebAssembly]]?
** "Low level bytecode that executes under virtual stack machine in browser"
** Portable target for compilation of high-level languages like C/C++, Rust
** Dizajniran od strane W3C, sa kontribucijama od Mozille, Microsoft-a, Google-a, Apple-a
* Kratka povijest
** Pocetkom 2017, izasao MVP, v1.0
** Krajem 2017, Mozilla declared support for all major browsers
** Sredinom 2018, ~85% [[https://developer.mozilla.org/en-US/docs/WebAssembly#Browser_compatibility][preglednika]] na svijetu podrzava wasm
* O wasm
** Prezentacija
| Type | Description                        |
|------+------------------------------------|
| wasm | binary format                      |
| wat  | WebAssembly Textual representation |

#+BEGIN_SRC rust
(module
  (func $mul_two (param $left i32) (param $right i32) (result i32)
    get_local $left
    get_local $right
    i32.mul)
  (export "mul_two" $mul_two)
)
#+END_SRC

** Tipovi podataka
| Type | Description           |
|------+-----------------------|
| i32  | 32-bit integer        |
| i64  | 64-bit integer        |
| f32  | 32-bit floating-point |
| f64  | 64-bit floating-point |
** Korisni alat [[https://github.com/WebAssembly/wabt][WABT]]
| Tool         | Description                                                                     |
|--------------+---------------------------------------------------------------------------------|
| wat2wasm     | translate from WebAssembly text format to the WebAssembly binary format         |
| wasm2wat     | translate from the binary format back to the text format (also known as a .wat) |
| wasm-objdump | print information about a wasm binary                                           |
| wasm-strip   | remove sections of a WebAssembly binary file                                    |
* Za web aplikacije
** [[https://github.com/koute/stdweb][stdweb]]
*** Idiomatski pristup
*** [[https://github.com/DenisKolodin/yew][Yew: stdweb framework, inspired by elm and react]]
** [[https://rustwasm.github.io/docs/wasm-bindgen][wasm-bindgen]]
*** JS/Rust varijanta
*** "hi level interaction between wasm and JS"
*** [[https://rustwasm.github.io/wasm-pack/book/][tool: Wasm Pack]] 
* Literatura
** Rust
*** "Programming Rust: Fast, Safe Systems Development" (Jim Blandy & Jason Orendorff) O'Reilly
*** "The Rust Programming Language" (Steve Klabnik & Carol Nichols)
*** https://doc.rust-lang.org/book/
** Rust + WASM
*** "Programming WebAssembly with Rust" (Kevin Hoffman)
*** https://rustwasm.github.io/book/
    
